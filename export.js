var _ = require('lodash');
var rest = require('restler');
var jsonfile = require('jsonfile');
var util = require('util');
var config = require('./config.json');
var moment = require('moment');


var tokenParam = '?token=' + config.token;

// generate file in output folder
var generateFile = function(messages, privateGroup,getAllNames) {
  // make an output file for this private group
  var outputFile = __dirname + config.outputDir + config.orgName + "_" + privateGroup.groupName + ".json";
  
  // convert each message to following info
  var convertedMessages =_.map(_.sortBy(messages,"ts"),function (value){

    // set time stamp to following format
    value.timeStamp = moment.unix(value.ts).format("ddd MMM D YYYY hh:mm A");
    delete value.ts;

    // if there a user exists for this message
    if (value.user != undefined) {
      // find the user's info
      value.userinfo = _.find(getAllNames,function(info){
        return info.id === value.user;
      }); 
      // set user to the name in returned user info
      value.user = value.userinfo.name;
      delete value.userinfo;
    }

    // if there is an inviter
    if (value.inviter != undefined) {
      // find inviter's info
      value.inviterinfo = _.find(getAllNames,function(info){
        return info.id === value.inviter;
      });

      // set inviter to the name in returned inviter info
      value.inviter = value.inviterinfo.name;
      delete value.inviterinfo;
    }

    // delete type (which is always "message")
    delete value.type;
    return value;
  });

  // create json files for the given private group
  jsonfile.writeFile(outputFile, convertedMessages, {
    spaces: 2
  }, function(err) {
    // if there is an error, print message with error
    if (err) {
      console.log("Error Writing file :: " + config.outputDir + config.orgName + "_" + privateGroup.groupName + ".json");
    }
    // else print message as generated
    else console.log("generated :: "+ config.outputDir + config.orgName + "_" + privateGroup.groupName + ".json");
  });
};

// Check if token is valid, if it is use that info for the private groups
rest.get(config.baseUrl + config.methods.auth + tokenParam).on('complete', function(data, response) {

  // if data returns error message, print message
  if (data instanceof Error) {
    console.log('Error:', data.message);
    this.retry(5000); // try again after 5 sec
  } else {

    // if data is not okay, print error message
    if (!data.ok) {
      console.log("Invalid Auth Token, please make sure the token is correct");
      process.exit(0);
    }

    //get the list of all groups -- used to generate the file later
    rest.get(config.baseUrl + config.methods.groups + tokenParam).on('complete', function(data, response) {
      
      // if data returns an error, print error message
      if (data instanceof Error) {
        console.log('Error:', data.message);
        this.retry(5000); // try again after 5 sec
      } else {

        // if data is not okay, print error message
        if (!data.ok) {
          console.log("Something is wrong, while retriving the list of groups");
          process.exit(0);
        }

        // since data is good, make allPGs map of group id, group name, members
        var allPGs = _.map(data.groups,function(value) {
          return {
            id: value.id,
            groupName: value.name,
            members: value.members
          }
        });

        // get data on users
        rest.get(config.baseUrl + config.methods.users + tokenParam).on('complete', function(data, response) {
          // if error on getting data, print error
          if (data instanceof Error) {
            console.log('Error:', data.message);
            this.retry(5000); // try again after 5 sec
          } else {

            // if data for users returns not ok, print error
            if (!data.ok) {
              console.log("Something is wrong, while retriving the list of users");
              return;
            }

            // since data is good, make userlist map of id's and names
            var userList = _.map(data.members, function(value) {
              return {
                id: value.id,
                name: value.name
              }
            });

            // for each private group, get the messages
            _.each(allPGs, function(gm) {
              var privateGroup = _.find(allPGs, function(u) {
                return u.id === gm.id;
              });
              if (!privateGroup)
                return;
              var getAllMessages = [];
              var groupMembers = privateGroup.members;

              // for each group member, get their name
              var getAllNames = [];
              _.each(groupMembers,function(mem){
                getName = _.find(userList,function(l) {
                  return l.id === mem;
                });
                getAllNames.push(getName);
              }); // end of each private group member loop
              
              // get all messages for this private group
              rest.get(config.baseUrl+config.methods.messages+tokenParam + "&count=1000&channel=" + gm.id).on('complete',function(allPGs,response){
                // if data returned is not okay, print error message
                if (!allPGs.ok) {
                  console.log('Error:', allPGs.messages);
                  process.exit(0);
                }

                // if the messages returned is 0, end of loop, move on to next private group
                if (allPGs.messages.length === 0)
                  return;

                // push all returned messages into getAllMessages array
                Array.prototype.push.apply(getAllMessages,allPGs.messages);

                // generate file
                generateFile(getAllMessages,privateGroup,getAllNames);
              }); // end of messages data

            }); // end of each private group loop 
          }
        });
       }
     })
  };
});

